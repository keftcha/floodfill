package main

import (
	"image"
	"image/color"
	"image/gif"

	"golang.org/x/exp/slices"
)

func contain(colors color.Palette, c color.RGBA) (bool, uint8) {
	for idx, clr := range colors {
		if clr == c {
			return true, uint8(idx)
		}
	}
	return false, 0
}

func extractPalette(img image.Image) color.Palette {
	palette := color.Palette{color.Transparent}
	for y := 0; y < img.Bounds().Max.Y; y++ {
		for x := 0; x < img.Bounds().Max.X; x++ {
			// Find the pixel color
			r, g, b, a := img.At(x, y).RGBA()
			c := color.RGBA{uint8(r), uint8(g), uint8(b), uint8(a)}
			// Add the pixel color if needed in our palette color
			if ok, _ := contain(palette, c); len(palette) == 0 || !ok {
				palette = append(palette, c)
			}
		}
	}
	return palette
}

func extractImageIntoPaletted(img image.Image) *image.Paletted {
	// Find image colors to make our color palette
	palette := extractPalette(img)
	// Create our first frame
	frame := image.NewPaletted(img.Bounds(), palette)

	model := frame.ColorModel()
	for y := 0; y < img.Bounds().Max.Y; y++ {
		for x := 0; x < img.Bounds().Max.X; x++ {
			idxColor := uint8(frame.Palette.Index(model.Convert(img.At(x, y))))
			frame.SetColorIndex(x, y, idxColor)
		}
	}

	return frame
}

func extractColorFromImage(img image.Image) *image.Paletted {
	// Find image colors to make our color palette
	palette := extractPalette(img)
	// Create our first frame
	frame := image.NewPaletted(img.Bounds(), palette)

	model := frame.ColorModel()
	for y := 0; y < img.Bounds().Max.Y; y++ {
		for x := 0; x < img.Bounds().Max.X; x++ {
			if (img.At(x, y) != color.RGBA{255, 255, 255, 255} && img.At(x, y) != color.RGBA{0, 0, 0, 255}) {
				idxColor := uint8(frame.Palette.Index(model.Convert(img.At(x, y))))
				frame.SetColorIndex(x, y, idxColor)
			}
		}
	}

	return frame
}

func copyImageIntoPaletted(img image.Image, frame *image.Paletted) {
	model := frame.ColorModel()
	for y := 0; y < img.Bounds().Max.Y; y++ {
		for x := 0; x < img.Bounds().Max.X; x++ {
			idxColor := uint8(frame.Palette.Index(model.Convert(img.At(x, y))))
			frame.SetColorIndex(x, y, idxColor)
		}
	}
}

// extractColoredPixels return a slice of pixels that are not black or white
// thoses are the pixels we'll need to see if we color neighbor
func extractColoredPixels(frame *image.Paletted) []image.Point {
	whiteIdx := uint8(frame.Palette.Index(color.RGBA{255, 255, 255, 255}))
	blackIdx := uint8(frame.Palette.Index(color.RGBA{0, 0, 0, 255}))
	cells := make([]image.Point, 0)

	for y := 0; y < frame.Bounds().Max.Y; y++ {
		for x := 0; x < frame.Bounds().Max.X; x++ {
			pixel := frame.ColorIndexAt(x, y)
			if pixel != whiteIdx && pixel != blackIdx {
				cells = append(cells, image.Pt(x, y))
			}
		}
	}

	return cells
}

func addFrame(g *gif.GIF, f *image.Paletted, d int) {
	g.Image = append(g.Image, f)
	g.Delay = append(g.Delay, d)
}

func hasBeenColored(g *gif.GIF, x, y int) bool {
	frames := g.Image[1:]

	for _, f := range frames {
		if (f.At(x, y) != color.Alpha16{}) {
			return true
		}
	}
	return false
}

func generateFloodfillGif(img image.Image, delay int) *gif.GIF {
	// Our output gif
	out := &gif.GIF{
		Image: make([]*image.Paletted, 0, 1),
		Delay: make([]int, 0, 1),
	}

	// The background of the gif is the png file
	backgroundFrame := extractImageIntoPaletted(img)
	addFrame(out, backgroundFrame, delay)

	// Initial pixels to check neighbor
	pixels := extractColoredPixels(backgroundFrame)

	frame := extractColorFromImage(img)
	// Loop while we have pixels to check if we need to color neighbor
	for len(pixels) != 0 {
		// The next frame we will color
		nextFrame := image.NewPaletted(frame.Bounds(), backgroundFrame.Palette)
		// Next pixels to check neighbor, thoses are the pixels we just have colored
		nextPixels := make([]image.Point, 0)

		// Fill the next frame
		for _, pix := range pixels {
			// Color idx in the palette is the color of the pixel we proceed
			colorIdx := uint8(frame.Palette.Index(frame.At(pix.X, pix.Y)))

			// Check the pixel above
			if 0 < pix.Y {
				backgroundAbove := backgroundFrame.At(pix.X, pix.Y-1)
				// If the above pixel is white on the background and
				// it's tranparent on all previous frames we color it
				if (backgroundAbove == color.RGBA{255, 255, 255, 255} && !hasBeenColored(out, pix.X, pix.Y-1)) {
					// Color the pixel
					nextFrame.SetColorIndex(pix.X, pix.Y-1, colorIdx)
					// Add the colored pixel to the list of pixel to process on next iteration
					point := image.Point{pix.X, pix.Y - 1}
					if !slices.Contains(nextPixels, point) {
						nextPixels = append(nextPixels, point)
					}
				}
			}

			// Check the pixel on the right
			if pix.X < backgroundFrame.Bounds().Max.X-1 {
				backgroundRight := backgroundFrame.At(pix.X+1, pix.Y)
				// If the pixel on the right is white on the background and
				// it's transparent on the previous frame we color it
				if (backgroundRight == color.RGBA{255, 255, 255, 255} && !hasBeenColored(out, pix.X+1, pix.Y)) {
					// Color the pixel
					nextFrame.SetColorIndex(pix.X+1, pix.Y, colorIdx)
					// Add the colored pixel to the list of pixel to process on next iteration
					point := image.Point{pix.X + 1, pix.Y}
					if !slices.Contains(nextPixels, point) {
						nextPixels = append(nextPixels, point)
					}
				}
			}

			// Check the pixel bellow
			if pix.Y < backgroundFrame.Bounds().Max.Y-1 {
				backgroundBellow := backgroundFrame.At(pix.X, pix.Y+1)
				// If the pixel bellow is white on the background and
				// it's transparent on the previous frame we color it
				if (backgroundBellow == color.RGBA{255, 255, 255, 255} && !hasBeenColored(out, pix.X, pix.Y+1)) {
					// Color the pixel
					nextFrame.SetColorIndex(pix.X, pix.Y+1, colorIdx)
					// Add the colored pixel to the list of pixel to process on next iteration
					point := image.Point{pix.X, pix.Y + 1}
					if !slices.Contains(nextPixels, point) {
						nextPixels = append(nextPixels, point)
					}
				}
			}

			// Check the pixel on the left
			if 0 < pix.X {
				backgroundLeft := backgroundFrame.At(pix.X-1, pix.Y)
				// If the pixel on the left is white on the background and
				// it's transparent on the previous frame we color it
				if (backgroundLeft == color.RGBA{255, 255, 255, 255} && !hasBeenColored(out, pix.X-1, pix.Y)) {
					// Color the pixel
					nextFrame.SetColorIndex(pix.X-1, pix.Y, colorIdx)
					// Add the colored pixel to the list of pixel to process on next iteration
					point := image.Point{pix.X - 1, pix.Y}
					if !slices.Contains(nextPixels, point) {
						nextPixels = append(nextPixels, point)
					}
				}
			}

		}
		pixels = nextPixels
		addFrame(out, nextFrame, delay)
		frame = nextFrame
	}

	return out
}
